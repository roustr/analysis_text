import re
class AnalysisService:
    #"\xe9" - e'
    #\xf1 - n'
    #\ c58b
    #teta - cfb4
    combinationsAlliteration = {
        "ca": "k",
        "co": "k",
        "cu": "k",
        "c" + chr(225): "k",
        "c" + chr(243): "k",
        "c" + chr(250): "k",
        "q": "k",
        "k": "k",
        "p": "p",
        "m": "m",
        "nm": "m",
        "nb": "m",
        "np": "m",
        "rr": "r",
        "r": "r",
        "l": "l",
        "ll": "l",
        "s": "s",
        "t": "t",
        "f": "f",
        "c": "ch",
       # "b": "b",
       # "v": "b",
       # "b": "v",
       # "v": "v",
        "d": "d",
        "cce":  "k" + chr(920),
        "cci":  "k" + chr(920),
        "cc" + chr(233): "k" + chr(920),
        "cc" + chr(237): "k" + chr(920),
        "z": chr(920),
        "ce": chr(920),
        "ci": chr(920),
        "c" + chr(233): chr(920),
        "c" + chr(237): chr(920),
        "x": "ks",
        "j": "j",
        "ge": "j",
        "gi": "j",
        "g" + chr(233): "j",
        "g" + chr(237): "j",
        "g": "g",
        "ng": chr(951),
        "nk": chr(951),
        "ll": chr(375),
        "y": chr(375),
        chr(241): chr(241),
    }
    combinationsAssonance = []

    def printAlliterations(self):
        print(self.combinationsAlliteration)

    def checkSoundInDictWords(self, words, length, sound):
        newDict = {"sound": sound, "words": []}
        for index, word in enumerate(words):
            if word.count(sound) > 0:
                newDict["words"].append({'word': word, 'index': length})
            length = length + 1
        return newDict

    def unionDict(self, dict1, dict2):
        dictAll = {}
        for key in dict1:
            dictAll[key] = [d[key] for d in (dict1, dict2)]
        return dictAll

    def test(self, text):
        print('.'.join(text))
        result = {"feature": []}
        for index, offer in enumerate(text):
            for sound in self.combinationsAlliteration:
                try:
                    temp2 = 0
                    if index + 1 != len(text):
                        temp2 = text[index + 1].count(sound)
                    temp1 = text[index].count(sound)
                    if temp1 > 0 and temp2 > 0:
                        countWordEndOffer = 1
                        countWordBeginOffer = 0
                        if index - 1 >= 0:
                            countWordEndOffer = index + 1
                            countWordBeginOffer = index
                        temp1_words = self.checkSoundInDictWords(
                            re.findall(r'\w+', text[index]),
                            len(re.findall(r'\w+', ''.join(text[0:countWordBeginOffer]))), sound)
                        temp2_words = self.checkSoundInDictWords(
                            re.findall(r'\w+', text[index + 1]),
                            len(re.findall(r'\w+', ''.join(text[0:countWordEndOffer]))), sound)
                        gty = self.unionDict(temp1_words, temp2_words)
                        #result["feature"].append()
                        #result["feature"].append(temp2_words)
                        print("Между двумя предложениями:", )
                        #print(temp1_words)
                        #print(temp2_words)
                        print(result["feature"])
                        print('[', len(re.findall(r'\w+', ''.join(text[0:countWordBeginOffer]))), ',', len(re.findall(r'\w+', ''.join(text[0:countWordEndOffer + 1]))) - 1, ']')
                        print('---------------')
                    elif temp1 > 0:
                        #print(result["feature"])
                        #continue
                        countWordBeginOffer = 0
                        if index - 1 >= 0:
                            countWordBeginOffer = index
                        resultCheckSound = self.checkSoundInDictWords(
                            re.findall(r'\w+', text[index]),
                            len(re.findall(r'\w+', ''.join(text[0:countWordBeginOffer]))), sound)
                        print("Одиночное предложение")
                        print(resultCheckSound)
                        print('-----------------------------')
                except:
                    break
